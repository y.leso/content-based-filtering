import React, {Component, Fragment} from 'react';

import {Container, Toast} from "react-bootstrap";
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faRss} from "@fortawesome/free-solid-svg-icons";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'semantic-ui-css/semantic.min.css'
import './App.css';

import Category from "./components/Category";
import Tags from "./components/Tags";
import Feed from "./components/Feed";
import Articles from "./ArticleRecommender";
import {Storage} from "./helpers";

export default class App extends Component {
	STEPS = {
		CATEGORIES: 0,
		TAGS: 1,
		RESULT: 2
	};

	constructor(props) {
		super(props);
		this.state = {...Storage.getValues(), modal: false};

		this.nextStep = this.nextStep.bind(this);
		this.prevStep = this.prevStep.bind(this);
		this.setModal = this.setModal.bind(this);
		this.Wrapper = this.Wrapper.bind(this);
	}

	componentDidMount() {Articles.setOpenModalFunction(this.setModal);}

	componentDidUpdate() {Storage.setValues(this.state);}

//----------------------------------------------------------------------------------------------------------------------
	Wrapper({children}) {
		const {modal} = this.state;
		const {title, subtitle, message} = modal;

		return <>
			<header>
				<Container>
					<Icon icon={faRss}/> Personal Feed
				</Container>
			</header>
			{children}
			{modal && <Toast onClose={() => this.setState({modal: false})} delay={5000} autohide>
				<Toast.Header>
					<img src="holder.js/20x20?text=%20" className="rounded mr-2" alt=""/>
					<strong className="mr-auto">{title}</strong>
					<small>{subtitle}</small>
				</Toast.Header>
				<Toast.Body>{message}</Toast.Body>
			</Toast>}
		</>
	}

	async setModal(title, subtitle, message) {
		await this.setState({modal: false});
		this.setState({modal: {title, subtitle, message}});
	}

//----------------------------------------------------------------------------------------------------------------------
	nextStep(type, state) {this.setState({step: this.state.step + 1, ...state})}

	prevStep(type, state) {this.setState({step: this.state.step - 1, ...state})}

//----------------------------------------------------------------------------------------------------------------------

	render() {
		const {step, categories, tags} = this.state;
		switch (step) {
			case this.STEPS.CATEGORIES:
				return <this.Wrapper>
					<Category onNext={this.nextStep}/>
				</this.Wrapper>;
			case this.STEPS.TAGS:
				return <this.Wrapper>
					<Tags onBack={this.prevStep}
						  onNext={this.nextStep}
						  openModal={this.setModal}/>
				</this.Wrapper>;
			case this.STEPS.RESULT:
				return <this.Wrapper>
					<Feed onBack={this.prevStep}
						  onNext={this.nextStep}
						  openModal={this.setModal}
						  criterias={{categories, tags}}/>
				</this.Wrapper>;
			default:
				throw Error("Step is not detected");
		}
	}
}

