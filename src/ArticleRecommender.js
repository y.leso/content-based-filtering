import {client, Storage} from "./helpers";
import App from "./App";

class ArticleRecommender {
	constructor() {
		this.articles = Storage.getValues("articles").articles || [];
		this.tags = [];
		this.recommendations = [];
		this.likedPriority = {};
		this.openModal = () => {};

		this.analyzeLiked();
	}

	async loadArticles() {
		const response = await client.getItems('article', {
			sort: "-created_on",
			fields: "title, url, brief, tags, categories, image.data.ful_url.*"
		});
		this.openModal("Article", "info", "Articles were loaded");

		Storage.setValues({articles: response.data}, "articles");
		return (this.articles = response.data);
	}

	async getRecomendations(categories, tags, minSimilarity = 0) {
		if (this.articles.length === 0) await this.loadArticles();

		this.recommendations = this.articles
			.map(
				article => ({
					...article,
					approach: this.calcSimilarity(categories, article.categories)
						+ 2 * this.calcSimilarity(tags, article.tags)
						+ this.calcSimilarityToLikes(article)
				})
			)
			.sort((a, b) => b.approach - a.approach)
			.filter(article => article.approach >= minSimilarity);

		return this.recommendations;
	}

	async getTags() {
		if (this.articles.length === 0) await this.loadArticles();

		return (this.tags = this.articles
				.map(({tags}) => tags.join(','))
				// join array of strings with tags in string format ("tag1, tag2, ...")
				.join(',')
				// splitting one big string of tags by delimiter
				.split(',')
				.unique()
		);
	}

	calcSimilarity(targetArray, fullArray) {
		// A ⋂ B
		const intersection = fullArray.filter(value => targetArray.includes(value));
		// A ⋃ B
		const union = [...new Set([...targetArray, ...fullArray])];

		// |A ⋂ B| / |A ⋃ B|
		return intersection.length / union.length;
	}

	calcSimilarityToLikes({url, categories, tags}) {
		if (!this.likedPriority) return 0;
		// to avoid setting a height similarity to articles that was liked. User has seem them
			/*|| this.likedPriority.liked.includes(url)*/

		const iterate = ({key, value}, type) => {
			if (type === "cats" && !categories.includes(key)) return;
			if (type === "tags" && !tags.includes(key)) return;

			similarity[type] += value;
			similarity.total += 1;
		};

		let similarity = {cats: 0, tags: 0, total: 0};


		this.likedPriority.categories.forEach((c) => iterate(c, "cats"));
		this.likedPriority.tags.forEach((t) => iterate(t, "tags"));
		return similarity.total > 0 ? (similarity.cats + similarity.tags) / similarity.total : 0;
	}

	analyzeLiked(likes = Storage.getValues("liked").liked || []) {
		let likedPriority = {
			categories: [],
			tags: [],
			liked: likes,
		};
		// grab all categories and tags from liked items
		this.articles.forEach(({url, categories, tags}) => {
			if (!likes.includes(url)) return;

			likedPriority = {
				...likedPriority,
				categories: likedPriority.categories.concat(categories),
				tags: likedPriority.tags.concat(tags)
			}
		});

		// count most liked categories and likes
		likedPriority.categories = likedPriority.categories.countRepeating();
		likedPriority.tags = likedPriority.tags.countRepeating();


		this.openModal("Likes", "info", `Your likes was analyzed by ${
		likedPriority.categories.length + likedPriority.tags.length
			} criterias`);
		return (this.likedPriority = likedPriority);
	}


	setOpenModalFunction(openModal) {
		this.openModal = openModal;
	}

	getLiked(liked = Storage.getValues("liked").liked){
		return this.articles.filter(({url}) => liked.includes(url));
	}

}

const Articles = new ArticleRecommender();
Articles.loadArticles();


export default Articles;