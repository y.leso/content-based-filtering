import DirectusSDK from "@directus/sdk-js";

//----------------------------------------------------------------------------------------------------------------------
export const EMPTY_STRING = "";
//----------------------------------------------------------------------------------------------------------------------

export const client = new DirectusSDK({
	url: "https://bi-vwm.netlify.app/admin",
	project: "the-cesko",
	storage: window.localStorage
});
//----------------------------------------------------------------------------------------------------------------------
export const Storage = {
	setValues: (values, type = EMPTY_STRING) => {
		localStorage.setItem(`storage.${type}`, JSON.stringify(values))
	},
	getValues: (type = EMPTY_STRING) => {
		const result = localStorage.getItem(`storage.${type}`);
		return result ? JSON.parse(result) : {
			categories: [],
			tags: [],
			liked: [],
			step: 0
		};
	}
};

export const articleUrl = (category, url) => `https://5e9852471912e500064ffc5d--the-cesko.netlify.app/read/${
	category.replace(" ", "_").toLowerCase()}/${url};`;

export const articleImg = (image) => `https://5e9852471912e500064ffc5d--the-cesko.netlify.app/imager/?url=${
	image.data.full_url}&w=300&h=260&fit=crop`;

//----------------------------------------------------------------------------------------------------------------------
Array.prototype.unique = function () {return [...new Set(this)];};

Array.prototype.removeValue = function (value) {return this.filter(v => v !== value);};

Array.prototype.removeByIndex = function (index) {return index > -1 ? this.splice(index, 1) : this;};

Array.prototype.countRepeating = function () {
	return this.reduce((acc, value) => ({
		...acc,
		[value]: (acc[value] || 0) + 1
	}), {}).toArray()
};

Object.prototype.toArray = function () {
	const result = [];
	for (let [key, value] of Object.entries(this))
		result.push({key, value});

	return result;
};
