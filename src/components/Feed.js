import React, {useState} from "react"
import ModuleComponent from "./ModuleComponent";
import Articles from "../ArticleRecommender";
import {Card, Container, Row} from "react-bootstrap";
import Footer from "./Footer";
import {Button, Header, Image, Modal, Card as SICard} from "semantic-ui-react";
import {articleImg, articleUrl, Storage} from "../helpers"

import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faArrowDown, faHeart} from "@fortawesome/free-solid-svg-icons";
import {faHeartBroken} from "@fortawesome/free-solid-svg-icons/faHeartBroken";


export default class Feed extends ModuleComponent {
	constructor(props) {
		super(props);
		this.state = {
			articles: [],
			liked: Storage.getValues("liked").liked || []
		};

		this.onLike = this.onLike.bind(this);
		this.myLikesComponent = this.myLikesComponent.bind(this);
	}

	onLike(url) {
		let {liked} = this.state;

		if (liked.includes(url)) liked = liked.removeValue(url);
		else liked.push(url);

		Storage.setValues({liked}, "liked");
		this.setState({liked});

	}

	componentDidMount() {
		const {categories, tags} = this.props.criterias;
		Articles.analyzeLiked();
		Articles.getRecomendations(categories, tags).then(articles => this.setState({articles}))
	}


	myLikesComponent({}) {
		const [open, setOpen] = useState(false);
		const {liked} = this.state;

		return <Modal
			trigger={<Button onClick={() => setOpen(true)} className="d-block m-auto">My Likes
				[{liked.length}]</Button>}
			open={open}
			onClose={() => setOpen(false)}
			basic
			size='small'
		>
			<Header icon='browser' content='Cookies policy'/>
			<Modal.Content>
				<SICard.Group>
				{Articles.getLiked(liked).map(({url, categories, title, brief, image}) => <SICard>
					<SICard.Content>
						<Image
							floated='right'
							sze='mini'
							src={articleImg(image)}
						/>
						<SICard.Header>{title}</SICard.Header>
						<SICard.Meta>{categories[0]}</SICard.Meta>
						<SICard.Description>
							<div dangerouslySetInnerHTML={{__html: brief}}/>
						</SICard.Description>
					</SICard.Content>
					<SICard.Content extra>
						<div className='ui two buttons'>
							<Button as="a" href={articleUrl(categories[0], url)} basic color='green'>
								Read
							</Button>
							<Button as="a" basic color='red' onClick={() => this.onLike(url)}>
								<Icon icon={faHeartBroken}/>
								Disslike
							</Button>
						</div>
					</SICard.Content>
				</SICard>)}
				</SICard.Group>
			</Modal.Content>
			<Modal.Actions>
				<Button color='green' onClick={() => setOpen(false)} inverted>
					<Icon icon={faArrowDown}/>
				</Button>
			</Modal.Actions>
		</Modal>

	}

	render() {
		const {articles, liked} = this.state;
		return <Container>
			<Row>
				{articles.map(({title, brief, image, url, categories}, key) => <Card key={key} style={{width: '18rem'}}>
					<Card.Img variant="top"
							  src={articleImg(image)}/>
					<Card.Body>
						<Card.Title>{title}</Card.Title>
						<Card.Text>
							<div dangerouslySetInnerHTML={{__html: brief}}/>
						</Card.Text>
						<div className="meta-splitter">
							<Button as="a"
									href={articleUrl(categories[0], url)}>
								Read
							</Button>
							<Button as="a" onClick={() => this.onLike(url)}
									className={liked.includes(url) ? "liked" : ""}>
								<Icon icon={faHeart}/>
							</Button>
						</div>
					</Card.Body>
				</Card>)}
			</Row>
			<Footer onBack={this.onBack} onNext={this.onNext}>
				<this.myLikesComponent/>
			</Footer>
		</Container>;
	}
}