import React from "react";
import {Button, Container, FormControl, InputGroup} from "react-bootstrap";
import {EMPTY_STRING, Storage} from "../helpers";
import Footer from "./Footer";
import ModuleComponent from "./ModuleComponent";
import Articles from "../ArticleRecommender";
import {Search} from "semantic-ui-react";

export default class Tags extends ModuleComponent {
	type = "tags";

	constructor(props) {
		super(props);
		this.state = {
			tags: Storage.getValues().tags || [],
			value: EMPTY_STRING,
			articles_tags: [],
			result: [],
			isLoading: false,
		};

		this.onTagWritten = this.onTagWritten.bind(this);
		this.onTagRemove = this.onTagRemove.bind(this);
		this.onTagAdd = this.onTagAdd.bind(this);
		this.onSearchChange = this.onSearchChange.bind(this);
	}

	componentDidMount() {
		Articles.getTags()
			.then(articles_tags => articles_tags.map(tag => ({title: tag})))
			.then(articles_tags => this.setState({articles_tags}))
			.then(() => this.props.openModal("Tags", "info", "Tag List has been loaded"))
	}

	onTagWritten({target}) {
		const tagsArray = target.value.split(',');
		const {tags} = this.state;
		const {openModal} = this.props;

		// if length is 1, we have no tags to add
		if (tagsArray.length <= 1)
			this.setState({value: target.value});
		else
			this.setState({
				value: EMPTY_STRING,
				tags: tags.concat(
					tagsArray.map(
						tag => tag
							.replace('#', EMPTY_STRING)
							.replace(/\s/g, '_')
							.trim()
					).filter(tag => {
						if (!tags.includes(tag))
							return tag.replace(/_/g, EMPTY_STRING) !== EMPTY_STRING;

						openModal("Tags", `#${tag}`, `This tags was already added`);
						return false;
					})
				)

			})
	}

	onSearchChange(e, {value}) {
		this.setState({isLoading: true, value});

		const {articles_tags} = this.state;
		this.setState({
			result: articles_tags.filter(({title}) => title.includes(value)),
			isLoading: false
		})
	}

	onTagRemove({target}) {
		let {tags} = this.state;
		this.setState({tags: tags.removeValue(target.innerText.replace("#", EMPTY_STRING))});
	}

	onTagAdd(event, {result}) {
		const {tags} = this.state;
		const {openModal} = this.props;

		if (tags.includes(result.title))
			openModal("Tags", `#${result.title}`, `This tags was already added`);
		else {
			tags.unshift(result.title);
			this.setState({tags, value: EMPTY_STRING});
		}
	}

	render() {
		const {tags, value, articles_tags, result, isLoading} = this.state;

		return <div className="full-page">
			<Container>
				<h1>Choose tags</h1>
				<Search
					input={{icon: 'tag', iconPosition: 'left'}}
					loading={isLoading}
					onResultSelect={this.onTagAdd}
					onSearchChange={this.onSearchChange}
					placeholder="Start entering tag to find best match"
					results={result}
					value={value}
					// resultRenderer={resultRenderer}
				/>
				<div className="scrolled">
					{tags.map((tag, index) => <Button
						key={index}
						variant="light"
						onClick={this.onTagRemove}
					>
						#{tag}
					</Button>)}
				</div>

				{tags.length > 0 && <Footer onBack={this.onBack} onNext={this.onNext}/>}
			</Container>
		</div>;
	}
}