import React, {Component} from "react";

export default class ModuleComponent extends Component{
	onNext = this.props.onNext
		? () => {this.props.onNext(this.type, this.state)}
		: false;

	onBack = this.props.onBack
		? () => {this.props.onBack(this.type, this.state)}
		: false
}