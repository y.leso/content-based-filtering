import React, {Fragment} from 'react';
import {Button, Container} from "react-bootstrap";
import {Categories} from "./../Categories";
import Footer from "./Footer";
import ModuleComponent from "./ModuleComponent";
import {Storage} from "./../helpers";


export default class Category extends ModuleComponent {
	type="categories";

	buttonVariants = [
		"primary",
		"secondary",
		"success",
		"warning",
		"light",
		"dark",
		"link"
	];

	constructor(props) {
		super(props);
		this.state = {
			categories: Storage.getValues().categories
		}
	}


	onCategoryClick(key) {
		const {categories} = this.state;
		const indexOfCat = categories.indexOf(key);

		if (indexOfCat > -1) categories.removeByIndex(indexOfCat);
		else categories.push(key);

		this.setState({categories})
	}

	render() {
		const {categories} = this.state;
		const CatList = Categories.map(
			({title, key}, index) => <Button
				key={index}
				className={categories.includes(key) && "active"}
				variant={this.buttonVariants[index % (this.buttonVariants.length - 1)]}
				onClick={() => this.onCategoryClick(key)}
			>
				{title}
			</Button>);

		return <Fragment>
			<div className="full-page">
				<Container>
					<h1>Choose categories</h1>
					<div className="categories-list">{CatList}</div>
					<div className="categories-list inverted">{CatList}</div>
				</Container>
			</div>
			{categories.length > 0 && <Footer onNext={this.onNext}>
				{categories.map(key => <Button variant="light">@{key}</Button>)}
			</Footer>}
		</Fragment>
	}
}

