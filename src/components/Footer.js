import React, {Component} from "react";
import {Button, Col, Container, Row} from "react-bootstrap";
import {FontAwesomeIcon as Icon} from "@fortawesome/react-fontawesome";
import {faArrowLeft, faArrowRight} from "@fortawesome/free-solid-svg-icons";

export default class Footer extends Component {

	render() {
		const {onBack, onNext, children} = this.props;
		const bodySize = 12 - 2*(+Boolean(onBack) + +Boolean(onNext));
		return <footer>
			<Container>
				<Row>
					{onBack && <Col sm={2}><Button onClick={onBack}>Back <Icon icon={faArrowLeft}/></Button></Col>}
					<Col sm={bodySize} className="scrolled">
						{children}
					</Col>
					{onNext && <Col sm={2}><Button onClick={onNext}>Next <Icon icon={faArrowRight}/></Button></Col>}
				</Row>
			</Container>
		</footer>
	}
}